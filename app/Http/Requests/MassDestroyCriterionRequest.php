<?php

namespace App\Http\Requests;

use App\Criterion;
use Gate;
use Illuminate\Foundation\Http\FormRequest;

class MassDestroyCriterionRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('criterion_delete'), 403, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:criteria,id',
        ];
    }
}
