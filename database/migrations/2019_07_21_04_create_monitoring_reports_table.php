<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMonitoringReportsTable extends Migration
{
    public function up()
    {
        Schema::create('monitoring_reports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('examiner_id');
            $table->foreign('examiner_id')->references('id')->on('users');
            $table->unsignedInteger('observer_id');
            $table->foreign('observer_id')->references('id')->on('users');
            $table->unsignedInteger('branch_id');
            $table->foreign('branch_id')->references('id')->on('branches');
            $table->dateTime('exam_date');
            $table->string('drivecategory');
            $table->date('observing_date');
            $table->string('observing_type');
            $table->longText('technical_note')->nullable();
            $table->longText('observer_note')->nullable();
            $table->longText('examiner_note')->nullable();
            $table->dateTime('examiner_reviewed')->nullable();
            $table->longText('evpis_note')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
