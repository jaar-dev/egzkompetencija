<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call([
            PermissionsTableSeeder::class,
            RolesTableSeeder::class,
            PermissionRoleTableSeeder::class,
            UsersTableSeeder::class,
            RoleUserTableSeeder::class,
            PointsTableSeeder::class,
            BranchesTableSeeder::class,
            CompetenciesSeeder::class,
            CriteriaSeeder::class,
        ]);
    }
}
